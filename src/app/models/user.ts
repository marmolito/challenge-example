export interface User {
    name: string,
    last_name?: string,
    email: string,
    document: number,
    gender: string,
    age?: number,
    hobbie: string,
}
