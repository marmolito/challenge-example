import { ExampleServices } from 'app/services/users/example.service';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';
import { User } from 'app/models/user';

@Component({
    selector     : 'sign-up-modern',
    templateUrl  : './sign-up.component.html',
    styleUrls: ['./sign-up.page.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class SignUpModernComponent implements OnInit
{
    @ViewChild('signUpNgForm') signUpNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signUpForm: FormGroup;
    showAlert: boolean = false;
    user:User;

    constructor(
        private exampleServices: ExampleServices,
        private _authService: AuthService,
        private _formBuilder: FormBuilder
    )
    {
    }
    ngOnInit(): void
    {
        this.getLocalStorage()
    }

    signUp(): void
    {
    }
    
    getLocalStorage() {
      this.user =  this.exampleServices.getUser();
    }
}
