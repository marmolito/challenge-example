import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmationService } from '@fuse/services/confirmation';
import { User } from 'app/models/user';
import { ExampleServices } from 'app/services/users/example.service';
import { ValidationService } from 'app/services/validations/validation.service';

@Component({
    selector: 'sign-up-classic',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.page.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
})
export class SignUpClassicComponent implements OnInit {
    signUpForm: FormGroup;
    createForm: FormGroup;
    configForm: FormGroup;
    showAlert: boolean = false;
    showAge: boolean = false;
    user: User;

    genders = [
        {
            id: 0,
            gender: 'Masculino',
        },
        {
            id: 1,
            gender: 'Femenino',
        },
    ];

    hobbies = [
        {
            id: 0,
            hobbie: 'Leer',
        },
        {
            id: 1,
            hobbie: 'Caminar',
        },
        {
            id: 2,
            hobbie: 'Dormir',
        },
    ];

    constructor(
        public formBuilder: FormBuilder,
        private validationService: ValidationService,
        private exampleServices: ExampleServices,
        private _fuseConfirmationService: FuseConfirmationService
    ) {}

    ngOnInit(): void {
        this.validateForm();
    }

    validateForm() {
        this.createForm = this.formBuilder.group({
            names: [
                '',
                [this.validationService.Validation(2, 51, 'onlyLetter', true)],
            ],
            last_names: [
                '',
                [this.validationService.Validation(2, 51, 'onlyLetter', false)],
            ],
            email: [
                '',
                [this.validationService.Validation(2, 51, 'onlyEmail', true)],
            ],
            document: [
                '',
                [this.validationService.Validation(9, 51, 'onlyNumber', true)],
            ],
            gender: ['', Validators.compose([Validators.required])],
            age: [
                '',
                [this.validationService.Validation(0, 4, 'onlyNumber', false), Validators.max(100)],
            ],
            hobbie: ['', Validators.compose([Validators.required])],
        });
    }

    validateDialogForm(message: string, icon: string) {
        this.configForm = this.formBuilder.group({
            title: 'Solicitud de usuario',
            message: '<span class="font-medium"> ' + message + ' </span>',
            icon: this.formBuilder.group({
                show: true,
                name: icon,
                color: 'primary',
            }),
            actions: this.formBuilder.group({
                confirm: this.formBuilder.group({
                    show: true,
                    label: 'Aceptar',
                    color: 'primary',
                }),
                cancel: this.formBuilder.group({
                    show: false,
                    label: 'Cancel',
                }),
            }),
            dismissible: true,
        });
    }

    saveLocalStorage() {
        let validation: boolean = this.exampleServices.createUser(
            this.getData()
        );
        let message = '';
        let icon = '';
        if (validation) {
            message = 'Usuario guardado en el LocalStorage';
            icon = 'heroicons_outline:check';
            this.createForm.reset();
            this.showAge = false;
        } else {
            message = 'Usuario no guardado en el LocalStorage';
            icon = 'heroicons_outline:exclamation-circle';
        }
        this.validateDialogForm(message, icon);
        this.openConfirmationDialog();
    }

    checkBygender(event: any): void {
        this.showAge = event.value == 'Masculino' ? true : false;
        this.createForm.get('age').setValue('');
    }
    

    getData() {
        let userArray: User = {
            name: this.createForm.get('names').value,
            last_name: this.createForm.get('last_names').value,
            email: this.createForm.get('email').value,
            document: this.createForm.get('document').value,
            gender: this.createForm.get('gender').value,
            age: this.createForm.get('age').value,
            hobbie: this.createForm.get('hobbie').value,
        };
        return userArray;
    }

    openConfirmationDialog(): void {
        const dialogRef = this._fuseConfirmationService.open(
            this.configForm.value
        );
        dialogRef.afterClosed().subscribe((result) => {
            console.log(result);
        });
    }

    get errorControl() {
        return this.createForm.controls;
    }
}
