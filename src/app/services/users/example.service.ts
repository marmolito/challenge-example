import { Injectable } from '@angular/core';
import { User } from 'app/models/user';

@Injectable({
    providedIn: 'root',
})
export class ExampleServices {
    constructor() {}

    createUser(userData: User) {
        localStorage.setItem('user', JSON.stringify(userData));
        console.log('usuario guardado en Local Storage');

        let user = this.getUser();
        if (user) {
            console.log('true');
            return true;
        } else {
            console.log('false ');
            return false;
        }
    }

    getUser() {
        let user = JSON.parse(localStorage.getItem('user'));
        console.log('usuario obtenido del Local Storage: ', user);
        return user;
    }
}
