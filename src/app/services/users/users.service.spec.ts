import { TestBed } from '@angular/core/testing';

import { ExampleServices } from './example.service';

describe('ExampleServices', () => {
    let service: ExampleServices;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(ExampleServices);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
