import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';

@Injectable({
    providedIn: 'root',
})
export class ValidationService {
    pattern = new RegExp('^[a-zA-ZñÑáéíóúÁÉÍÓÚs ]+$');
    onlyLetters = new RegExp('^[a-zA-ZñÑáéíóúÁÉÍÓÚs ]+$');
    onlyNumber = new RegExp(/^([0-9])*$/);
    onlyEmail = new RegExp(
        /^(?!.*[Ã±Ã‘])(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    anyText = new RegExp(
        '^[_A-Za-z0-9-ZñÑáéíóúÁÉÍÓÚs@,+()-.$!%*?&/¡¿#"´`¨°¬\':;{}<=> ]+$'
    );
    constructor() {}
    Validation(min: number, max: number, option: string, requerid?: boolean) {
        return (control: AbstractControl): { [key: string]: any } | null => {
            if (option == 'onlyLetter') {
                this.pattern = this.onlyLetters;
            } else if (option == 'onlyEmail') {
                this.pattern = this.onlyEmail;
            } else if (option == 'onlyNumber') {
                this.pattern = this.onlyNumber;
            } else if (option == 'anyText') {
                this.pattern = this.anyText;
            }

            if (
                requerid == false &&
                control.value != null &&
                control.value.length == 0
            ) {
                return null;
            } else if (control.value != null && control.value.length == 0) {
                return { nameRequerid: true };
            } else if (control.value != null && control.value.length <= min) {
                return { nameNumberMin: true };
            } else if (control.value != null && control.value.length >= max) {
                return { nameNumberMax: true };
            } else if (!this.pattern.test(control.value)) {
                return { nameText: true };
            }

            return null;
        };
    }
}
